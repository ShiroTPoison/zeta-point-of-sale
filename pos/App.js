import "react-native-gesture-handler";
import React, { useEffect, useState } from "react";
import { Provider as StoreProvider } from "react-redux";
import configureStore from "./src/redux/store";
import AppNavigator from "./src/navigator/navigation";
// import { StatusBar } from "expo-status-bar";

export default function App() {
  return (
    <StoreProvider store={configureStore()}>
      {/* <StatusBar style="auto" /> */}
      <AppNavigator />
    </StoreProvider>
  );
}
