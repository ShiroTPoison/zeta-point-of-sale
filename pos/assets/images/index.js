export const images = {
  dashboard: require("./icons/dashboard.png"),
  bg: require("./bg.png"),
  auth: {
    logoDark: require("../images/auth/logoDark.png"),
    logoWhite: require("../images/auth/logoWhite.png"),
    onboard1: require("../images/auth/onboard1.png"),
    onboard2: require("../images/auth/onboard2.png"),
    onboard3: require("../images/auth/onboard3.png"),
  },
};

export const icons = {
  bell: require("./icons/bell.png"),
  home: require("./icons/home.png"),
  educate: require("./icons/educate.png"),
  plane: require("./icons/plane.png"),
  reactnative: require("./icons/react-native.png"),
  logo: require("./icons/logo.png"),
};
