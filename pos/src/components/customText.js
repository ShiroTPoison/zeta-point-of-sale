import React from "react";
import { StyleSheet } from "react-native";
import {
  Title,
  Text,
  Subheading,
  Headline,
  Caption,
  Paragraph,
  withTheme,
} from "react-native-paper";

class CText extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Text style={[styles.defaultStyle, this.props.style]}>
        {this.props.children}
      </Text>
    );
  }
}
export const CustomText = withTheme(CText);

class CTitle extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Title style={[styles.defaultStyle, this.props.style]}>
        {this.props.children}
      </Title>
    );
  }
}
export const CustomTitle = withTheme(CTitle);

class CHeadline extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Headline style={[styles.defaultStyle, this.props.style]}>
        {this.props.children}
      </Headline>
    );
  }
}
export const CustomHeadline = withTheme(CHeadline);

class CSubheading extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Subheading style={[styles.defaultStyle, this.props.style]}>
        {this.props.children}
      </Subheading>
    );
  }
}
export const CustomSubheading = withTheme(CSubheading);

class CParagraph extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Paragraph style={[styles.defaultStyle, this.props.style]}>
        {this.props.children}
      </Paragraph>
    );
  }
}
export const CustomParagraph = withTheme(CParagraph);

class CCaption extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Caption style={[styles.defaultStyle, this.props.style]}>
        {this.props.children}
      </Caption>
    );
  }
}
export const CustomCaption = withTheme(CCaption);

const styles = StyleSheet.create({
  defaultStyle: {
    // fontFamily: `Poppins_400Regular`,
    textAlign: "center",
  },
});
