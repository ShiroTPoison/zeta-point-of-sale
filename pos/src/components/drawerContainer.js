import React from "react";
import { StyleSheet, View } from "react-native";
import MenuButton from "./menuBtn";
import { AppIcon } from "../AppStyles";
import auther from "@react-native-firebase/auth";
import { useDispatch } from "react-redux";

export default function DrawerContainer({ navigation }) {
  const dispatch = useDispatch();
  return (
    <View style={styles.content}>
      <View style={styles.container}>
        <MenuButton
          title="LOG OUT"
          source={AppIcon.images.logout}
          onPress={() => {
            navigation.navigate("LoginStack");
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  container: {
    flex: 1,
    alignItems: "flex-start",
    paddingHorizontal: 20,
  },
});
