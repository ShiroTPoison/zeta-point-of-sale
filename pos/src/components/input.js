import * as React from "react";
import { TextInput, useTheme } from "react-native-paper";

const inputComponent = (props) => {
  const [text, setText] = React.useState("");
  const { colors } = useTheme();
  function onChange(v) {
    props.onChange(v);
    setText(props);
  }
  return (
    <TextInput
      mode="outlined"
      label="Email"
      value={text}
      onChangeText={(text) => setText(text)}
    />
  );
};

export default inputComponent;
