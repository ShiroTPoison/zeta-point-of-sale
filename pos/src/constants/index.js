import React, { useEffect, useRef } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const useSearchFilter = (data, searchText) => {
  const [filteredData, setFilteredData] = useState([]);
  useEffect(() => {
    const stringifiedObjectList = data.map((itemObj) =>
      JSON.stringify(itemObj).toLowerCase()
    );
    // filter on the string array. Just like searching a string
    const filteredStringifiedObjectList = stringifiedObjectList.filter(
      (itemObj) => itemObj.includes(searchtext.toLowerCase())
    );
    const filtered = filteredStringifiedObjectList.map((item) =>
      JSON.parse(item)
    );
    setFilteredData(filtered);
  }, [searchText]);

  return filteredData;
};

export const setStorageItem = async (key, value) => {
  try {
    return await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    console.error(e);
    return false;
  }
};

export const getStorageItem = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    console.error(e);
    return false;
    // error reading value
  }
};
