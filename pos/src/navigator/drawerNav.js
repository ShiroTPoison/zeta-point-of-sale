import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from "@react-navigation/drawer";
import styles from "../styles/navCss";
import TabNavigator from "./tabNav";

const Drawer = createDrawerNavigator();
// DrawerNavigation = () => {
//   return (
//     <Drawer.Navigator
//       //   screenOptions={{
//       //     headerTitleStyle: styles.fontPoppins,
//       //     headerTitleAlign: "center",
//       //     headerTintColor: "#FFFFFF",
//       //     drawerItemStyle: styles.drawerItem,
//       //     drawerStyle: styles.drawer,
//       //     headerStyle: { backgroundColor: primaryMain },
//       //     drawerActiveBackgroundColor: secondaryMain,
//       //     drawerActiveTintColor: "white",
//       //     drawerInactiveTintColor: "black",
//       //   }}
//       //   drawerContent={(props) => (
//       //     <this.CustomDrawerContent
//       //       session={this.props.submitLoginReducer.data.session}
//       //       profileUrl={this.props.submitLoginReducer.data.user_pp}
//       //       logout={this.props.logout}
//       //       {...props}
//       //     />
//       //   )}
//       initialRouteName="Dashboard"
//     >
//       <Drawer.Screen
//         options={{
//           //   headerTitleStyle: styles.fontPoppins,
//           //   drawerLabelStyle: styles.fontPoppins,
//           //   headerRight: this.headerRight,
//           headerTransparent: true,
//           //   headerTitleContainerStyle: { height: 0, width: 0 },
//           //   drawerIcon: ({ focused, size }) => (
//           //     <Image
//           //       source={images.dashboard}
//           //       style={[focused ? null : null, { height: size, width: size }]}
//           //     />
//           //   ),
//         }}
//         name="Dashboard"
//         component={Dashboard}
//       />
//     </Drawer.Navigator>
//   );
// };

const DrawerNav = () => (
  <Drawer.Navigator
    screenOptions={{
      drawerStyle: [styles.drawer, { outerWidth: 200 }],
      drawerPosition: "left",
      headerShown: false,
    }}
    // drawerContent={({ navigation }) => (
    //   <DrawerContainer navigation={navigation} />
    // )}
  >
    <Drawer.Screen name="HomeDrawer" component={TabNavigator} />
  </Drawer.Navigator>
);

export default DrawerNav;
