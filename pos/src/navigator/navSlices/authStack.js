import React, { useEffect, useState, useRef } from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Styles from "../../styles/navCss";
import WellcomeScreen from "../../screens/wellcome";
import LoginScreen from "../../screens/login";
import { getStorageItem } from "../../constants";
import LanguageScreen from "../../screens/wellcome/language";
const Stack = createNativeStackNavigator();

const AuthStack = (props) => {
  const [ready, setready] = useState(false);
  const board = useRef(false);

  useEffect(async () => {
    board.current = await getStorageItem("onboard");
    setready(true);
  }, []);

  if (!ready) return null;
  return (
    <Stack.Navigator
      // initialRouteName={onboard ? "Login" : "Wellcome"}
      initialRouteName={board.current ? "Language" : "Language"}
      screenOptions={{
        headerShown: false,
        headerTintColor: "red",
        headerTitleStyle: Styles().headerTitleStyle,
        headerMode: "float",
      }}
    >
      <Stack.Screen name="Language" component={LanguageScreen} />
      <Stack.Screen name="Wellcome" component={WellcomeScreen} />
      <Stack.Screen name="Login" component={LoginScreen} />
      {/* <Stack.Screen name="Signup" component={SignupScreen} /> */}
    </Stack.Navigator>
  );
};

export default AuthStack;
