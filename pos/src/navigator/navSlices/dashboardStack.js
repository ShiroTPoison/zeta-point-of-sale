import React, { useEffect } from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import styles from "../../styles/navCss";
import DashboardScreen from "../../screens/dashboard";
import { Ionicons } from "@expo/vector-icons";
import images from "../../../assets/images";
import Playground from "../../screens/playground";
import * as Location from "expo-location";

const Stack = createNativeStackNavigator();
const DashboardStack = (props) => {
  // useEffect(async () => {
  //   let { status } = await Location.requestForegroundPermissionsAsync();
  //   return () => {
  //     status;
  //   };
  // }, []);
  return (
    <Stack.Navigator
      initialRouteName="DashboardScreen"
      screenOptions={{
        headerTintColor: "red",
        headerTitleStyle: styles.headerTitleStyle,
        headerMode: "float",
      }}
    >
      <Stack.Screen
        name="DashboardScreen"
        component={DashboardScreen}
        options={({ navigation }) => ({
          // headerLeft: () => (
          //   <Pressable onPress={() => navigation.openDrawer()}>
          //     <Image source={images.dashboard} />
          //   </Pressable>
          // ),
          // headerLeftContainerStyle: { paddingLeft: 10 },
        })}
      />
      <Stack.Screen name="Playground" component={Playground} />
    </Stack.Navigator>
  );
};

export default DashboardStack;
