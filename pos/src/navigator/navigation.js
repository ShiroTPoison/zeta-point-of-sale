import React, { lazy, Suspense, useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Appearance, View } from "react-native";
import { connect } from "react-redux";
import { StatusBar } from "expo-status-bar";
import AppLoading from "expo-app-loading";
// import { setAppAppearance } from "../redux/common/actions/actions";
import { paperLightTheme, paperDarkTheme } from "../styles/theme";
import { Provider as PaperProvider } from "react-native-paper";
const load = (Component) => (props) =>
  (
    <Suspense
      fallback={
        <View style={{ flex: 1, alignItems: "center" }}>
          <AppLoading onError={console.warn} />
        </View>
      }
    >
      <Component {...props} />
    </Suspense>
  );
const AuthStack = load(lazy(() => import("./navSlices/authStack")));
const DrawerNav = load(lazy(() => import("./drawerNav")));

// import AuthStack from "./navSlices/authStack";
// import DrawerNav from "./drawerNav";

const Stack = createNativeStackNavigator();
const RootNavigator = (props) => (
  <Stack.Navigator
    initialRouteName={true ? "AuthStack" : "DrawerNav"}
    screenOptions={{ headerShown: false }}
  >
    <Stack.Screen name="AuthStack" component={AuthStack} />
    <Stack.Screen name="DrawerNav" component={DrawerNav} />
  </Stack.Navigator>
);

class AppNavigator extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.unsubscribe;
  // }

  state = {
    ready: false,
    theme: paperLightTheme,
    mounted: false,
  };
  _changeTheme(c) {
    if (c == "dark") {
      this.setState({ theme: paperDarkTheme });
    } else {
      this.setState({ theme: paperLightTheme });
    }
  }
  _toggleTheme = () => {
    this.state.theme.dark
      ? this._changeTheme("dark")
      : this._changeTheme("light");
  };
  _themeListener = ({ colorScheme }) => {
    this._changeTheme(colorScheme || "light");
  };

  componentDidMount() {
    // await this.props.setAppAppearance(Appearance.getColorScheme() || "light");
    this._changeTheme(Appearance.getColorScheme() || "light");
    this.setState({ ready: true });
    const unsubscribe = Appearance.addChangeListener(this._themeListener);
    return () => {
      unsubscribe?.remove(this._themeListener);
    };
  }
  // componentWillUnmount() {
  //   this.unsubscribe?.remove(this._themeListener);
  // }
  render() {
    if (this.state.ready) {
      return (
        <PaperProvider theme={this.state.theme}>
          <NavigationContainer>
            <RootNavigator />
          </NavigationContainer>
        </PaperProvider>
      );
    } else return null;
  }
}

function mapStateToProps(state) {
  return {
    // appAppearance: state.appAppearanceReducer
  };
}
export default connect(mapStateToProps, {
  // setAppAppearance: setAppAppearance,
})(AppNavigator);
