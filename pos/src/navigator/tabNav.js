import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { icons } from "../../assets/images";
import styles from "../styles/navCss";
import { navigatorsColors } from "../styles/theme";
import DashboardStack from "./navSlices/dashboardStack";

const BottomTab = createBottomTabNavigator();
const TabNavigator = () => (
  <BottomTab.Navigator
    initialRouteName="DashboardTab"
    screenOptions={{
      tabBarInactiveTintColor: navigatorsColors.tabInactive,
      tabBarActiveTintColor: navigatorsColors.tabActive,
      tabBarIcon: ({ focused }) => {
        return (
          <Image
            style={[
              {
                tintColor: focused
                  ? navigatorsColors.tabActive
                  : navigatorsColors.tabInactive,
              },
              styles().tabImage,
            ]}
            source={icons.logo}
          />
        );
      },
      headerShown: false,
    }}
  >
    <BottomTab.Screen
      options={{ tabBarLabel: "DashboardTab" }}
      name="DashboardTab"
      component={DashboardStack}
    />
  </BottomTab.Navigator>
);

export default TabNavigator;
