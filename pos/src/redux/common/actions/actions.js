import * as types from "../constants/constants";
import axios from "axios";
import { ServerUrl } from "../../helper/helper";
import { Alert } from "react-native";

export function logout() {
  return async (dispatch) => {
    dispatch({ type: types.RESET_ACTION });
  };
}

export function setAppAppearance(value) {
  return async (dispatch) => {
    dispatch({ type: types.APP_APPEARANCE, payload: value });
  };
}

export function setLocation(value) {
  return async (dispatch) => {
    dispatch({ type: types.LOCATION, payload: value });
  };
}

export function setAddress(value) {
  return async (dispatch) => {
    dispatch({ type: types.ADDRESS, payload: value });
  };
}

export function setLanguage(value) {
  return async (dispatch) => {
    dispatch({ type: types.LANGUAGE, payload: value });
  };
}
