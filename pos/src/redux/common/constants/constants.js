export const RESET_ACTION = "RESET_ACTION";

export const APP_APPEARANCE = "APP_APPEARANCE";

export const LOCATION = "LOCATION";
export const ADDRESS = "ADDRESS";

export const LANGUAGE = "LANGUAGE";
