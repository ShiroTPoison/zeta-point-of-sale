import setAppApearanceReducer from "./setAppApearanceReducer";
import locationReducer from "./locationReducer";
import addressReducer from "./addressReducer";
import languageReducer from "./languageReducer";

const reducers = {
  appAppearanceReducer: setAppApearanceReducer,
  addressReducer: addressReducer,
  locationReducer: locationReducer,
  languageReducer: languageReducer,
};
export default reducers;
