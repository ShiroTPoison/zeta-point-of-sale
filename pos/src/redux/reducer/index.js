import { combineReducers } from "redux";
import loginReducers from "../../screens/login/reducers";
import commonReducers from "../common/reducers";
const rootReducer = (state, action) => {
  if (action.type === "RESET_ACTION") {
    //Reseting Redux Store ( LogOut )
    return appReducer(undefined, action);
  }

  return appReducer(state, action);
};
const appReducer = combineReducers({
  ...loginReducers,
  ...commonReducers,
});
export default rootReducer;
