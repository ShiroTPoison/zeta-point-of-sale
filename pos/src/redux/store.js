import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import rootReducers from "./reducer";
import { ServiceMiddleware } from "./middleware/serviceMiddleWare.js";

export default function configureStore() {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  return createStore(
    rootReducers,
    composeEnhancers(applyMiddleware(reduxThunk, ServiceMiddleware))
  );
}
