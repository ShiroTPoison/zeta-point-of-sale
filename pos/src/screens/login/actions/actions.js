import * as types from "../constants/constants";
import axios from "axios";
import { ServerUrl } from "../../../redux/helper/helper";
import { Alert } from "react-native";

export function submitLoginAccount(data) {
  return async (dispatch) => {
    dispatch({ type: types.LOGIN_ACCOUNT_ATTEMPT });
    var formData = new FormData();
    formData.append("email", data.email);
    formData.append("password", data.password);
    formData.append("deviceModelName", data.deviceModelName);

    var config = {
      method: "post",
      url: `${ServerUrl}Login/Login`,
      headers: {
        // "Content-Type": "multipart/form-data",
        "Content-Type": "application/json",
      },
      // data: formData,
      data: {
        userName: "075",
        password: "zAmAN!21",
        userID: 0,
      },
    };

    await axios(config)
      .then(function (response) {
        dispatch({
          type: types.LOGIN_ACCOUNT_SUCCESS,
          payload: response.data,
        });
        // console.log("login/", response.data);
        return response.data;
      })
      .catch(function (error) {
        console.error("error///", error); // Console Log
        Alert.alert("Error! Logging in was unsucessfull", `${error}`);
        dispatch({ type: types.LOGIN_ACCOUNT_FAIL, payload: error });
        throw new Error(error);
      });
  };
}
