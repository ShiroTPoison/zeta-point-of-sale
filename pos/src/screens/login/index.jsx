import React from "react";
import { StyleSheet, Text, View } from "react-native";
import gloabalStyle from "../../styles/index";

export default function LoginScreen() {
  const gStyle = gloabalStyle();
  return (
    <View style={[gStyle.container, gStyle.bg]}>
      <View style={[gStyle.content]}>
        <Text style={gStyle.text}>
          Open up App.js to start working on your app!
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
