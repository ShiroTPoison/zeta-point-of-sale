import React, { useState } from "react";
import { connect } from "react-redux";
import gloabalStyle from "../../styles/index";
import { colorDictionary } from "../../styles/theme";
import AppIntroSlider from "react-native-app-intro-slider";
import wellcomeConfig from "./wellcomeConfig";
import {
  getCurrentAddress,
  getGeoCode,
  getRevGeoCode,
  changeRevGeoCodeToText,
} from "../../constants/location";
import { setStorageItem } from "../../constants/index";
import { ExpandingDot } from "react-native-animated-pagination-dots";
import { setLocation, setAddress } from "../../redux/common/actions/actions";
import {
  CustomText,
  CustomTitle,
  CustomParagraph,
} from "../../components/customText";
import {
  StyleSheet,
  View,
  Image,
  useColorScheme,
  Animated,
  Text,
  TouchableOpacity,
} from "react-native";
import { Surface, useTheme, Snackbar, Button } from "react-native-paper";

const circleSize = 60;

function WellcomeScreen({ navigation, addressReducer, setAddressAction }) {
  let appearance = useColorScheme();
  const { colors } = useTheme();
  const gStyle = gloabalStyle(appearance);
  const style = styles(appearance);
  const scrollX = React.useRef(new Animated.Value(0)).current;
  const [snackMsg, setsnackMsg] = useState(false);
  const [visibleSnack, setVisibleSnack] = useState(false);
  const onDismissSnackBar = () => setVisibleSnack(false);

  function navigate() {
    setStorageItem("onboard", true);
    navigation.reset({
      index: 0,
      routes: [{ name: "Login" }],
    });
  }
  const showLocationErr = (v) => {
    setsnackMsg(v);
    setVisibleSnack(true);
  };
  async function getCurrentLocation() {
    getCurrentAddress()
      .then(async (v) => {
        setAddressAction(v);
      })
      .catch((e) => {
        showLocationErr(e.message);
      });
  }
  const slides = wellcomeConfig.onboardingConfig.walkthroughScreens.map(
    (screenSpec, index) => {
      return {
        key: `${index}`,
        text: screenSpec.description,
        title: screenSpec.title,
        image: screenSpec.icon,
        button: index == 2 && screenSpec.button,
        subTitle: index == 2 && screenSpec.subTitle,
      };
    }
  );
  const _renderLabelButton = (v) => {
    return <CustomTitle>{v}</CustomTitle>;
  };
  const _renderItem = ({ item, dimensions, index }) => (
    <View
      style={[
        gStyle.container,
        // gStyle.jcCentre,
        gStyle.itmCentre,
        dimensions,
        {
          backgroundColor: colorDictionary.colorSet[appearance].background,
          flexDirection: "column-reverse",
        },
      ]}
    >
      {index == 2 && (
        <View style={style.locationView}>
          <Button onPress={getCurrentLocation}>
            <CustomTitle style={{ color: colors.primary }}>
              {item.button}
            </CustomTitle>
          </Button>
          <TouchableOpacity>
            <CustomParagraph>{item.subTitle}</CustomParagraph>
          </TouchableOpacity>
        </View>
      )}
      <Surface style={[style.surface, index == 2 && { marginBottom: 0 }]}>
        <View style={[style.circle, style.rightBgCircle]} />
        <View style={[style.circle, style.leftSmallCircle]} />
        <CustomText
          style={[
            style.wellcomText,
            { color: colorDictionary.colorSet["dark"].onBackground },
          ]}
        >
          {item.text}
        </CustomText>
        <ExpandingDot
          data={slides}
          expandingDotWidth={30}
          scrollX={scrollX}
          inActiveDotOpacity={0.6}
          activeDotColor={"#FFFFFF"}
          inActiveDotColor={"#FFFFFF"}
          dotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 5,
          }}
        />
      </Surface>
      {index == 2 && (
        <CustomText
          style={[
            style.wellcomText,
            {
              color: colorDictionary.colorSet[appearance].onBackground,
              top: 20,
            },
          ]}
        >
          {item.title}
        </CustomText>
      )}
      <Image
        style={style.image}
        resizeMethod="scale"
        resizeMode="contain"
        source={item.image}
      />
    </View>
  );

  return (
    <>
      <AppIntroSlider
        data={slides}
        slides={slides}
        renderItem={_renderItem}
        onDone={navigate}
        showSkipButton={true}
        showDoneButton={false}
        showNextButton={true}
        dotStyle={{ width: 0, height: 0 }}
        activeDotStyle={{ width: 0, height: 0 }}
        renderNextButton={() => _renderLabelButton("Next")}
        renderDoneButton={() => _renderLabelButton("Done")}
        renderSkipButton={() => _renderLabelButton("Skip")}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          {
            useNativeDriver: false,
          }
        )}
      />
      <Snackbar
        visible={visibleSnack}
        onDismiss={onDismissSnackBar}
        duration={5000}
        action={{
          onPress: () => {
            // Do something
          },
        }}
      >
        {snackMsg}
      </Snackbar>
    </>
  );
}

function mapStateToProps(state) {
  return {
    // appAppearance: state.appAppearanceReducer,
    addressReducer: state.addressReducer,
  };
}

export default connect(mapStateToProps, {
  setAddressAction: setAddress,
  setLocationAction: setLocation,
})(WellcomeScreen);

export const styles = (props) =>
  StyleSheet.create({
    wellcomeTitle: {
      fontSize: 25,
      fontWeight: "bold",
      textAlign: "center",
      paddingBottom: 25,
      color: colorDictionary.colorSet[props].onBackground,
    },
    image: {
      height: 338,
      marginBottom: 60,
      // width: "100%",
      // tintColor: colorDictionary.colorSet[props].primary,
    },
    wellcomText: {
      fontSize: 18,
      textAlign: "center",
      color: colorDictionary.colorSet[props].onBackground,
      paddingLeft: 10,
      paddingRight: 10,
    },
    surface: {
      padding: 20,
      minHeight: 160,
      width: 350,
      alignItems: "center",
      backgroundColor: colorDictionary.colorSet[props].primary,
      justifyContent: "center",
      elevation: 4,
      borderRadius: 25,
      marginBottom: 120,
    },
    circle: {
      position: "absolute",
      backgroundColor: "white",
      opacity: 0.2,
    },
    rightBgCircle: {
      top: 13,
      right: 20,
      height: circleSize,
      width: circleSize,
      borderRadius: circleSize,
    },
    leftSmallCircle: {
      bottom: 29,
      left: 20,
      height: circleSize / 2,
      width: circleSize / 2,
      borderRadius: circleSize / 2,
    },
    locationView: {
      height: 120,
      justifyContent: "center",
    },
  });
