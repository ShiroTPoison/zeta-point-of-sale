import React from "react";
import { Image, StyleSheet, View, useColorScheme } from "react-native";
import { setStorageItem } from "../../constants/index";
import { images } from "../../../assets/images/index";
import gloabalStyle from "../../styles/index";
import { colorDictionary } from "../../styles/theme";
import { useTheme, Button } from "react-native-paper";
import { CustomSubheading } from "../../components/customText";
import { CachedImage } from "../../components/ImageCached";
import { setLanguage } from "../../redux/common/actions/actions";

export default function Language({ navigation }) {
  let appearance = useColorScheme();
  const { colors } = useTheme();
  const gStyle = gloabalStyle(appearance);
  const style = styles(appearance);
  function changeLanguage(v) {
    setStorageItem("language", v);
    setLanguage(v);
    navigation.navigate("Wellcome");
  }
  let item = {};
  return (
    <View style={style.container}>
      <CachedImage
        source={{
          uri: `https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg`,
        }}
        cacheKey={`12345`}
        resizeMode="contain"
        style={style.image}
      />
      <Image
        resizeMode="contain"
        source={
          appearance == "dark" ? images.auth.logoWhite : images.auth.logoDark
        }
        style={[style.image]}
      />

      <CustomSubheading style={style.text}>
        Please select your language
      </CustomSubheading>
      <Button
        mode="contained"
        onPress={() => changeLanguage("enlgish")}
        style={style.button}
      >
        Enlgish
      </Button>
      <Button
        mode="contained"
        onPress={() => changeLanguage("urdu")}
        style={style.button}
      >
        Urdu
      </Button>
    </View>
  );
}

const styles = (props) =>
  StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colorDictionary.colorSet[props].background,
      paddingHorizontal: 40,
      justifyContent: "center",
    },
    image: {
      //   width: "100%",
      width: 280,
    },
    button: {
      backgroundColor: colorDictionary.colorSet[props].secondary,
      marginTop: 15,
    },
    text: {
      marginTop: 20,
    },
  });
