import { images } from "../../../assets/images";
const wellcomeConfig = {
  onboardingConfig: {
    walkthroughScreens: [
      {
        icon: images.auth.onboard1,
        title: "Signup",
        description: "Go for a wide range of 5000+ products.",
      },
      {
        icon: images.auth.onboard2,
        title: "Management",
        description: "Get your Product within 30 minutes.",
      },
      {
        icon: images.auth.onboard3,
        title: `Zeta requires access to your \nlive location`,
        button: "Use My Current Location",
        subTitle: "Set Location Manually",
        description:
          "We would like to access your location so you can view products near by you.",
      },
    ],
  },
};

export default wellcomeConfig;
