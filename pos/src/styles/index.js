import { StyleSheet } from "react-native";
import { navigatorsColors } from "./theme";
import { useColorScheme } from "react-native";
import { colorDictionary } from "./theme";

const styles = () => {
  const appearance = useColorScheme();
  if (appearance == undefined || appearance == null) {
    appearance = "light";
  }
  return StyleSheet.create({
    headerTitleStyle: {
      fontWeight: "bold",
      textAlign: "center",
      alignSelf: "center",
      color: "black",
    },
    drawer: {},
    iconStyle: { tintColor: navigatorsColors.tabActive, width: 30, height: 30 },
    tabImage: {},
    container: {
      flex: 1,
    },
    bg: { backgroundColor: colorDictionary.colorSet[appearance].background },
    jcCentre: {
      justifyContent: "center",
    },
    itmCentre: {
      alignItems: "center",
    },
    text: {
      color: colorDictionary.colorSet[appearance].onBackground,
    },
    content: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginHorizontal: 18,
      marginVertical: 20,
    },
  });
};

export default styles;
