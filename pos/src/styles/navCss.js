import { StyleSheet, useColorScheme } from "react-native";
import { navigatorsColors } from "./theme";
import { colorDictionary } from "./theme";

const styles = (appearance) => {
  return StyleSheet.create({
    headerTitleStyle: {
      fontWeight: "bold",
      textAlign: "center",
      alignSelf: "center",
      color: "black",
    },
    drawer: {},
    iconStyle: { tintColor: navigatorsColors.tabActive, width: 30, height: 30 },
    tabImage: {},
    jcCentre: {
      justifyContent: "center",
    },
  });
};

export default styles;
