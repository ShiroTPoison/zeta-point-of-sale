import { DarkTheme, DefaultTheme } from "react-native-paper";

export const navigatorsColors = {
  tabInactive: "grey",
  tabActive: "orange",
};

const lightColorSet = {
  primary: "#EF4E36",
  primaryVariant: "#AE1F00",
  secondary: "#2C327E",
  muted: "grey",
  error: "#CF6679",
  background: "#FFFFFF",
  surface: "#F4F4F4",
  onPrimary: "#FFFFFF",
  onSecondary: "#FFFFFF",
  onBackground: "#000000",
  onSurface: "#000000",
  onError: "#000000",
  shadow: "black",
};

const darkColorSet = {
  primary: "#EF4E36",
  primaryVariant: "#AE1F00",
  secondary: "#2C327E",
  muted: "grey",
  error: "#CF6679",
  background: "#121212",
  surface: "#474747",
  onPrimary: "#FFFFFF",
  onSecondary: "#FFFFFF",
  onBackground: "#FFFFFF",
  onSurface: "#FFFFFF",
  onError: "#FFFFFF",
  shadow: "black",
};

const colorSet = {
  ...lightColorSet,
  light: lightColorSet,
  dark: darkColorSet,
  "no-preference": lightColorSet,
  null: lightColorSet,
};

export const colorDictionary = {
  colorSet,
};

export const paperLightTheme = {
  ...DefaultTheme,
  dark: false,
  roundness: 4,
  colors: {
    primary: lightColorSet.primary,
    accent: lightColorSet.primaryVariant,
    background: lightColorSet.background,
    surface: lightColorSet.surface,
    error: lightColorSet.error,
    text: lightColorSet.onBackground,
    onSurface: lightColorSet.onSurface,
    disabled: lightColorSet.muted,
    placeholder: lightColorSet.muted,
    backdrop: lightColorSet.shadow,
    notification: lightColorSet.secondary,
  },
  // fonts: configureFonts(),
  animation: {
    scale: 2.0,
  },
};

export const paperDarkTheme = {
  ...DarkTheme,
  dark: true,
  roundness: 4,
  mode: "adaptive",
  colors: {
    primary: darkColorSet.primary,
    accent: darkColorSet.primaryVariant,
    background: darkColorSet.background,
    surface: darkColorSet.surface,
    error: darkColorSet.error,
    text: darkColorSet.onBackground,
    onSurface: darkColorSet.onSurface,
    disabled: darkColorSet.muted,
    placeholder: darkColorSet.muted,
    backdrop: darkColorSet.shadow,
    notification: darkColorSet.secondary,
  },
  // fonts: configureFonts(),
  animation: {
    scale: 2.0,
  },
};
